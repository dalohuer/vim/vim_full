set number
set relativenumber
set mouse=a
set numberwidth=1
set clipboard=unnamedplus
syntax enable
set showcmd
set ruler
set encoding=utf-8
set showmatch
set sw=2
set laststatus=2
set noshowmode
set shiftwidth=4 "- indenting is 4 spaces
set autoindent   "- turns it on
set cindent 	 "- stricter rules for C programs t
set foldenable
set foldmethod=indent
set hlsearch
set incsearch      " incremental searching
set ignorecase     " searches are case insensitive...
set smartcase      " ... unless they contain at least one capital letter
set list lcs=tab:\|\
"para colocar automaticamente linecursor sin tenerlo a todo momento
autocmd InsertEnter,InsertLeave * set cul!
"para cambiar la manera en que se ve el cursor al editar
" let &t_SI = "\<Esc>]50;CursorShape=1\x7"
" let &t_EI = "\<Esc>]50;CursorShape=0\x7"

"parametros de prueba
:set tabstop=4     "- tabs are at proper location
:set expandtab     "- don't use actual tab character (ctrl-v)
:set smartindent   "- does the right thing (mostly) in programs
:set pastetoggle=<f5>
"configuración de fzf
set rtp+=~/.fzf
"Vim DevIcons
set conceallevel=3
" so ~/.vim/pluginsVundle.vim
so ~/.vim/pluginsPlug.vim

"set cursorline
"set listchars=tab:\┆\
"
"parametros de prueba
set colorcolumn=81
set smarttab
set showtabline=4
set formatoptions-=cro
"set hidden
set fileencoding=utf-8
set splitbelow
set splitright
set background=dark
syntax on
filetype on
filetype indent on
filetype plugin on

"Atajos de comandos con el teclado
nmap <Leader>w :w<CR>
nmap <Leader>q :q<CR>
nmap <Leader>x :x<CR>
