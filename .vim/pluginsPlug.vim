"Para hacer el llamado al plugings
call plug#begin('~/.vim/plugged')

"Temas
Plug 'morhetz/gruvbox'
Plug 'shinchu/lightline-gruvbox.vim'

"IDE--------------------------------------------------------
Plug 'editorconfig/editorconfig-vim'
Plug 'terryma/vim-multiple-cursors'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'turbio/bracey.vim' "para hacer la previsualización de html
"Busqueda de archivos
Plug 'easymotion/vim-easymotion'
Plug 'mhinz/vim-signify'
Plug 'yggdroot/indentline'
"Para comentar o descomentar lineas
Plug 'scrooloose/nerdcommenter'

"------------------------------------------------------------
"tree abre nerdtree (navegador de archivos)
Plug 'scrooloose/nerdtree'

"tmux
Plug 'benmills/vimux'
Plug 'christoomey/vim-tmux-navigator' 

"Autocompletado
Plug 'sirver/ultisnips'
Plug 'neoclide/coc.nvim', {'branch': 'release'}

"Tema del status bar de airline
"Plug 'vim-airline/vim-airline'
"Plug 'vim-airline/vim-airline-themes'  " Temas para airline

" status bar
Plug 'maximbaz/lightline-ale'
Plug 'itchyny/lightline.vim'

""Para colocar el tagbar
Plug 'preservim/tagbar'

"Iconos de nerdtree
Plug 'ryanoasis/vim-devicons'

" Plug 'garbas/vim-snipmate' "Autocompletado 
" Plug 'honza/vim-snippets'

"typing
"Plug 'jiangmiao/auto-pairs'
Plug 'alvan/vim-closetag'
Plug 'tpope/vim-surround'

"git
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'

call plug#end()

colorscheme gruvbox
let g:gruvbox_contrast_dark = "hard"

"Configuraciones NerdTree
let NERDTreeShowHidden=1
let NERDTreeQuitOnOpen=1 "para que el NerdTree se cierre cada vez que se abra un archivo
let NERDTreeAutoDeleteBuffer=1
let NERDTreeMinimalUI=1
let NERDTreeDirArrows=1
let NERDTreeShowLineNumbers=1
let NERDTreeMapOpenInTab='\t'

"tecla lider
let mapleader=" "

"para llamar el tagbar
nmap <F8> :TagbarToggle<CR>

"Desactivar el mensaje de alerta del autocompletado CoC
let g:coc_disable_startup_warning = 1

" Lightlane
let g:lightline = {
      \ 'active': {
      \   'left': [['mode', 'paste'], [], ['relativepath', 'modified']],
      \   'right': [['kitestatus'], ['filetype', 'percent', 'lineinfo'], ['gitbranch']]
      \ },
      \ 'inactive': {
      \   'left': [['inactive'], ['relativepath']],
      \   'right': [['bufnum']]
      \ },
      \ 'component': {
      \   'bufnum': '%n',
      \   'inactive': 'inactive'
      \ },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#head',
      \   'kitestatus': 'kite#statusline'
      \ },
      \ 'colorscheme': 'gruvbox',
      \ 'subseparator': {
      \   'left': '',
      \   'right': ''
      \ }
      \}
    "

"Barra de estado:
"let g:airline#extensions#tabline#enabled = 1  " Mostrar buffers abiertos (como pestañas)
"let g:airline#extensions#tabline#fnamemod = ':t'  " Mostrar sólo el nombre del archivo
"let g:airline_theme='minimalist'
" Cargar fuente Powerline y símbolos (ver nota)
"let g:airline_powerline_fonts = 1

"set noshowmode  " No mostrar el modo actual (ya lo muestra la barra de estado)

"atajo easymotion
nmap <Leader>s <Plug>(easymotion-s2)

"Atajo para abrir NerdTree
nmap <Leader>nt :NERDTreeFind<CR>
map <Leader>p :Files<CR>
map <Leader>ag :Ag<CR>

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" tabs navigation
map <Leader>h :tabprevious<cr>
map <Leader>l :tabnext<cr>

" git
nnoremap <Leader>G :G<cr>
nnoremap <Leader>gp :Gpush<cr>
nnoremap <Leader>gl :Gpull<cr>

"Atajos de comandos con el teclado
nmap <Leader>w :w<CR>
nmap <Leader>q :q<CR>
nmap <Leader>x :x<CR>

"Configuración ventana fzf
let g:fzf_layout = { 'left': '~30%'}

"mapeo de teclas para fzf
nmap <F2> :call fzf#run(fzf#wrap({'source': 'git ls-files --exclude-standard --others --cached'}))<Enter>
nmap <F3> :Buffers<Enter>

"Comandos para el manejo de selección de copiado
let g:highlightedyank_highlight_duration = 500

"Comandos para el manejo de NerdCommenter

" " Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'

" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1

" Add your own custom formats or override the defaults
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }

" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1

" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1

" Enable NERDCommenterToggle to check all selected lines is commented or not 
let g:NERDToggleCheckAllLines = 1

"VIm Devicons
let g:webdevicons_enable = 1
let g:webdevicons_enable_nerdtree = 1
let g:webdevicons_conceal_nerdtree_brackets = 1
let g:WebDevIconsUnicodeDecorateFolderNodes = 1
let g:DevIconsEnableFoldersOpenClose = 1
let g:WebDevIconsNerdTreeBeforeGlyphPadding = ''

"" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsSnippetDirectories=[$HOME.'/config/.vim/UltiSnips']
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<S-tab>"

"configuration indentline
let g:indentLine_setColors = 0
let g:indentLine_char_list = ['|', '¦', '┆', '┊']


" coc
"autocmd FileType python let b:coc_suggest_disable = 1
"autocmd FileType javascript let b:coc_suggest_disable = 1
"autocmd FileType scss setl iskeyword+=@-@

" vim fugitive
command! -bang -nargs=? -complete=dir GFiles
  \ call fzf#vim#gitfiles(<q-args>, fzf#vim#with_preview(), <bang>0)

command! -bang -nargs=* Ag
  \ call fzf#vim#ag(<q-args>, fzf#vim#with_preview(), <bang>0)

command! -bang -nargs=? -complete=dir Files
  \ call fzf#vim#files(<q-args>, fzf#vim#with_preview(), <bang>0)

" if hidden is not set, TextEdit might fail.
set hidden

" Some servers have issues with backup files, see #649
set nobackup
set nowritebackup

" Better display for messages
set cmdheight=1

" You will have bad experience for diagnostic messages when it's default 4000.
set updatetime=300

" don't give |ins-completion-menu| messages.
set shortmess+=c

" always show signcolumns
set signcolumn=yes

" fugitive always vertical diffing
set diffopt+=vertical



" Highlight symbol under cursor on CursorHold


let $FZF_DEFAULT_OPTS='--layout=reverse'
"let g:fzf_layout = { 'window': 'call FloatingFZF()' }
"function! FloatingFZF()
  "let buf = nvim_create_buf(v:false, v:true)
  "call setbufvar(buf, '&signcolumn', 'no')
  "let height = float2nr((&lines - 3) / 2)
  "let width = float2nr(&columns - (&columns * 2 / 10))
  "let col = float2nr((&columns - width) / 2)
  "let row = float2nr((&lines - height) / 2)
  "let opts = {
        "\ 'relative': 'editor',
        "\ 'row': row,
        "\ 'col': col,
        "\ 'width': width,
        "\ 'height': height
        "\ }
  "call nvim_open_win(buf, v:true, opts)
"endfunction

"function! SearchPatternInFile(pattern)
    "" Save cursor position.
    "let save_cursor = getcurpos()

    "" Set cursor position to beginning of file.
    "call cursor(0, 0)

    "" Search for the string 'hello' with a flag c.  The c flag means that a
    "" match at the cursor position will be accepted.
    "let search_result = search(a:pattern, "c")

    "" Set the cursor back at the saved position.  The setpos function was
    "" used here because the return value of getcurpos can be used directly
    "" with it, unlike the cursor function.
    "call setpos('.', save_cursor)

    "" If the search function didn't find the pattern, it will have
    "" returned 0, thus it wasn't found.  Any other number means that an instance
    "" has been found.
    "return search_result
"endfunction

