" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')
let g:clang_c_options = '-std=gnu11' 

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
Plugin 'https://github.com/christoomey/vim-tmux-navigator.git'

"para comentar textos
Plugin 'preservim/nerdcommenter'

" arbol de directorios, con espacio  toggle
Plugin 'https://github.com/scrooloose/nerdtree'

"Para colocar el tagbar
Plugin 'preservim/tagbar'

nmap <F8> :TagbarToggle<CR>

"Tecla lider es la barra espaciadora
let g:mapleader=" "

" Cambia el directorio actual al nodo padre actual
let g:NERDTreeChDirMode = 2 

let g:NERDTreeQuitOnOpen = 1

" Abrir/cerrar NERDTree con espacio + nt 
nmap <Leader>nt :NERDTreeToggle<CR>

"Atajos con el teclado 

nmap <Leader>w :w<CR>
nmap <Leader>q :q<CR>
nmap <Leader>x :x<CR>

"Barra de estado:
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'  " Temas para airline
let g:airline#extensions#tabline#enabled = 1  " Mostrar buffers abiertos (como pestañas)
let g:airline#extensions#tabline#fnamemod = ':t'  " Mostrar sólo el nombre del archivo

" Cargar fuente Powerline y símbolos (ver nota)
let g:airline_powerline_fonts = 1
set noshowmode  " No mostrar el modo actual (ya lo muestra la barra de estado)

"Autocompletado
Plugin 'garbas/vim-snipmate'
Plugin 'honza/vim-snippets'

"Configuración ventana fzf
let g:fzf_layout = { 'left': '~30%'}

"mapeo de teclas para fzf
nmap <F2> :call fzf#run(fzf#wrap({'source': 'git ls-files --exclude-standard --others --cached'}))<Enter>
nmap <F3> :Buffers<Enter>

" All of your Plugins must be added before the following line

call vundle#end()            " required

filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on

" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

"Comandos para el manejo de NerdCommenter

" " Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'

" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1

" Add your own custom formats or override the defaults
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }

" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1

" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1

" Enable NERDCommenterToggle to check all selected lines is commented or not 
let g:NERDToggleCheckAllLines = 1
