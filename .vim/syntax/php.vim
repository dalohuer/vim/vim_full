<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">

<head>
  <link rel="Stylesheet" type="text/css" href="/css/style.css" >
  <title>php.vim - PHP Syntax : vim online</title>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <meta name="KEYWORDS" content="Vim, Vi IMproved, text editor, home, documentation, tips, scripts, news">
  <meta name="viewport" content="width=1000, initial-scale=1">
  <link rel="shortcut icon" type="image/x-icon" href="/images/vim_shortcut.ico">
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"> 

<!-- HEADER, SPONSOR IMAGE, VIM IMAGE AND BOOK AD -->
<table width="100%" cellpadding="0" cellspacing="0" border="0" bordercolor="red">
  <tr>
    <td colspan="4" class="lightbg"><img src="/images/spacer.gif" width="1" height="5" alt=""></td>
  </tr>
  <tr>
  <td class="lightbg">&nbsp;&nbsp;&nbsp;</td>
  <td class="lightbg" align="left"><a href="https://www.vim.org/sponsor/index.php"><img src="/images/sponsorvim.gif" alt="sponsor Vim development" border="0"></a></td>
  <td class="lightbg" align="center">
	 <a href="/"><img src="/images/vim_header.gif" border="0" alt="Vim logo" class="align-middle"></a>
	  </td>
  <td class="lightbg" align="right"><a href="http://iccf-holland.org/vim_books.html"><img src="/images/buyhelplearn.gif" alt="Vim Book Ad" border="0"></a></td>
  </tr>
  <tr>
    <td colspan="4" class="lightbg"><img src="/images/spacer.gif" width="1" height="5" alt=""></td>
  </tr>
  <tr>
    <td colspan="4" class="darkbg"><img src="/images/spacer.gif" width="1" height="10" alt=""></td>
  </tr>
</table>
<!-- THE PAGE BODY: BETWEEN HEADER AND FOOTER -->

<table cellpadding="0" cellspacing="0" border="0" width="100%">
  <col width="180">
  <col width="1">

  <tr valign="top">
    <td class="sidebar">
      <table width="180" cellpadding="4" cellspacing="0" border="0">
        <tr valign="top">
          <td class="sidebar">

<!-- INCLUDE THE PAGE NAVIGATION -->
<table width="100%" cellpadding="0" cellspacing="0" border="0" bordercolor="red">
    <tr>
        <td><small>not logged in (<a href="https://www.vim.org/login.php">login</a>)</small></td>
    </tr>
    <tr><td>
<small>&nbsp;</small>
<form action="https://www.google.com/cse" id="cse-search-box">
  <div>
    <input type="hidden" name="cx" value="partner-pub-3005259998294962:bvyni59kjr1" />
    <input type="hidden" name="ie" value="ISO-8859-1" />
    <input type="text" name="q" size="20" />
    <br>
    <input type="submit" name="sa" value="Search" />
  </div>
</form>
<script type="text/javascript" src="https://www.google.com/coop/cse/brand?form=cse-search-box&amp;lang=en"></script>
    </td></tr>
    <tr>
        <td><img src="/images/spacer.gif" alt="" border="0" width="1" height="1"></td>
    </tr>
    <tr>
        <td class="darkbg"><img src="/images/spacer.gif" alt='' border="0" height="3"></td>
    </tr>
    <tr>
        <td><img src="/images/spacer.gif" alt="" border="0" width="1" height="2"></td>
    </tr>
        <tr>
            <td class="sidebarheader"><a href="https://www.vim.org/">Home</a></td>
        </tr>
        <tr>
            <td class="sidebarheader"><a href="https://www.vim.org/search.php">Advanced search</a></td>
        </tr>
    <tr>
        <td><img src="/images/spacer.gif" alt="" border="0" width="1" height="7"></td>
    </tr>
    <tr>
        <td class="checker"><img src="/images/spacer.gif" alt='' border="0" height="1"></td>
    </tr>
    <tr>
        <td><img src="/images/spacer.gif" alt="" border="0" width="1" height="7"></td>
    </tr>
        <tr>
            <td class="sidebarheader"><a href="https://www.vim.org/about.php">About Vim</a></td>
        </tr>
        <tr>
            <td class="sidebarheader"><a href="https://www.vim.org/community.php">Community</a></td>
        </tr>
        <tr>
            <td class="sidebarheader"><a href="https://www.vim.org/news/news.php">News</a></td>
        </tr>
        <tr>
            <td class="sidebarheader"><a href="https://www.vim.org/sponsor/index.php">Sponsoring</a></td>
        </tr>
        <tr>
            <td class="sidebarheader"><a href="https://www.vim.org/trivia.php">Trivia</a></td>
        </tr>
        <tr>
            <td class="sidebarheader"><a href="https://www.vim.org/docs.php">Documentation</a></td>
        </tr>
        <tr>
            <td class="sidebarheader download"><a href="https://www.vim.org/download.php">Download</a></td>
        </tr>
    <tr>
        <td><img src="/images/spacer.gif" alt="" border="0" width="1" height="7"></td>
    </tr>
    <tr>
        <td class="checker"><img src="/images/spacer.gif" alt='' border="0" height="1"></td>
    </tr>
    <tr>
        <td><img src="/images/spacer.gif" alt="" border="0" width="1" height="7"></td>
    </tr>
        <tr>
            <td class="sidebarheader"><a href="https://www.vim.org/scripts/index.php">Scripts</a></td>
        </tr>
        <tr>
            <td class="sidebarheader"><a href="https://www.vim.org/tips/index.php">Tips</a></td>
        </tr>
        <tr>
            <td class="sidebarheader"><a href="https://www.vim.org/account/index.php">My Account</a></td>
        </tr>
    <tr>
        <td><img src="/images/spacer.gif" alt="" border="0" width="1" height="7"></td>
    </tr>
    <tr>
        <td class="checker"><img src="/images/spacer.gif" alt='' border="0" height="1"></td>
    </tr>
    <tr>
        <td><img src="/images/spacer.gif" alt="" border="0" width="1" height="7"></td>
    </tr>
        <tr>
            <td class="sidebarheader"><a href="https://www.vim.org/huh.php">Site Help</a></td>
        </tr>
</table>
<br>

            <table width="172" cellpadding="0" cellspacing="0" border="0">
              <tr><td><img src="/images/spacer.gif" alt="" border="0" width="1" height="8"></td></tr>
              <tr><td class="darkbg"><img src="/images/spacer.gif" width="1" height="3" alt=""></td></tr>
            </table>
            <br>

<!-- INCLUDE THE PAGE SIDEBAR TEXT -->
&nbsp;

          </td>
        </tr>
      </table>
    </td>

    <td class="darkbg"><img src="/images/spacer.gif" width="1" height="1" border="0" alt=""><br></td>
    <td>
      <table width="100%" cellpadding="10" cellspacing="0" border="0" bordercolor="red">
        <tr>
          <td valign="top">

<span class="txth1">php.vim : PHP Syntax</span> 

<br>
<br>

<!-- karma table -->
<table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066">
<tr>
  <td class="lightbg"><b>&nbsp;script karma&nbsp;</b></td>
  <td>
    Rating <b>822/351</b>,
    Downloaded by 53130  </td>
  <td class="lightbg">
  <b>&nbsp;Comments, bugs, improvements&nbsp;</b>
  </td>
  <td>
    <a href="http://vim.wikia.com/wiki/Script:1571">Vim wiki</a>
  </td>  
</tr>
</table>
<p>

<table cellspacing="0" cellpadding="0" border="0">
<tr><td class="prompt">created by</td></tr>
<tr><td><a href="/account/profile.php?user_id=10023">Peter Hodge</a></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td class="prompt">script type</td></tr>
<tr><td>syntax</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td class="prompt">description</td></tr>
<tr><td>PHP syntax file, with several bug fixes, improvements and additions.&nbsp;&nbsp;Should be up-to-date with all features of PHP 5.1.4.&nbsp;&nbsp;If anything is missing, broken, not working well on your setup, or an inconvenience for you, please let me know, all feedback is welcome: &lt;toomuchphp-vim at yahoo.com&gt;.<br><br><br>New and improved highlighting features include:<br>===============================================<br><br>- User-defined special functions (__autoload(), __construct(), etc)<br>- Built-in constants (STDIN, STR_PAD_LEFT, etc)<br>- Built-in classes (stdClass, Exception, Reflection, etc)<br>- Built-in interfaces (Iterator, Serializable, etc)<br>- Complex expressions inside double-quoted strings using { }<br>- Support for PCRE patterns<br>&nbsp;&nbsp;(Patterns are only recognized if the pattern is inside the<br>&nbsp;&nbsp;function call.)<br>- Braces are colored to help you see when they are mismatched.<br><br><br>* You can find a complete list of available options near the top of the syntax file.<br><br>* Release 0.9 is the official 'stable' release, however I do encourage you to use the latest version as it shouldn't have any bugs either.</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td class="prompt">install details</td></tr>
<tr><td>Copy php.vim to:<br>&nbsp;&nbsp;~/.vim/syntax/php.vim<br>&nbsp;&nbsp;&nbsp;&nbsp;or<br>&nbsp;&nbsp;c:\program files\vim\vimfiles\syntax\php.vim</td></tr>
<tr><td>&nbsp;</td></tr>
</table>

<!-- rating table -->
<form name="rating" method="post">
<input type="hidden" name="script_id" value="1571">
<table cellpadding="4" cellspacing="0" border="1" bordercolor="#000066">
<tr>
  <td class="lightbg"><b>rate this script</b></td>
  <td valign="middle">
    <input type="radio" name="rating" value="life_changing">Life Changing
    <input type="radio" name="rating" value="helpful">Helpful
    <input type="radio" name="rating" value="unfulfilling">Unfulfilling&nbsp;
    <input type="submit" value="rate">
  </td>
</tr>
</table>
</form>
<span class="txth2">script versions</span> (<a href="add_script_version.php?script_id=1571">upload new version</a>)
<p>
Click on the package to download.
<p>

<table cellspacing="2" cellpadding="4" border="0" width="100%">
<tr class='tableheader'>
        <th valign="top">package</th>
    <th valign="top">script version</th>
    <th valign="top">date</th>
    <th valign="top">Vim version</th>
    <th valign="top">user</th>
    <th valign="top">release notes</th>
</tr>
<tr>
        <td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=8651">php.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.9.7</b></td>
    <td class="rowodd" valign="top" nowrap><i>2008-05-06</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=10023">Peter Hodge</a></i></td>
    <td class="rowodd" valign="top" width="2000">Many small bug fixes and improvements</td>
</tr>
<tr>
        <td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=6341">php.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>0.9.6</b></td>
    <td class="roweven" valign="top" nowrap><i>2006-10-27</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=10023">Peter Hodge</a></i></td>
    <td class="roweven" valign="top" width="2000">Highlights ';' errors in many more places, can now fold array() structures as well.</td>
</tr>
<tr>
        <td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=6326">php.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.9.5</b></td>
    <td class="rowodd" valign="top" nowrap><i>2006-10-23</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=10023">Peter Hodge</a></i></td>
    <td class="rowodd" valign="top" width="2000">New colors for { } around class/function/try/catch code blocks.&nbsp;&nbsp;Better errors when a brace or semicolon is out of place.&nbsp;&nbsp;Options can be configured globally or separately for each buffer.</td>
</tr>
<tr>
        <td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=6046">php.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>0.9.4</b></td>
    <td class="roweven" valign="top" nowrap><i>2006-08-07</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=10023">Peter Hodge</a></i></td>
    <td class="roweven" valign="top" width="2000">Now supports multi-line patterns and double-quoted patterns (but no variable substitution yet)</td>
</tr>
<tr>
        <td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=5976">php.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.9.3</b></td>
    <td class="rowodd" valign="top" nowrap><i>2006-07-20</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=10023">Peter Hodge</a></i></td>
    <td class="rowodd" valign="top" width="2000">Improved support for PCRE patterns, fixed variable substituion in strings, added option to highlight '-&gt;' in an alternate color when used for object properties.</td>
</tr>
<tr>
        <td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=5861">syntax.tar.gz</a></td>
    <td class="roweven" valign="top" nowrap><b>0.9.2</b></td>
    <td class="roweven" valign="top" nowrap><i>2006-06-25</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=10023">Peter Hodge</a></i></td>
    <td class="roweven" valign="top" width="2000">Improved support for preg* functions, added support for most built-in constants</td>
</tr>
<tr>
        <td class="rowodd" valign="top" nowrap><a href="download_script.php?src_id=5836">syntax.tar.gz</a></td>
    <td class="rowodd" valign="top" nowrap><b>0.9.1</b></td>
    <td class="rowodd" valign="top" nowrap><i>2006-06-15</i></td>
    <td class="rowodd" valign="top" nowrap>6.0</td>
    <td class="rowodd" valign="top"><i><a href="/account/profile.php?user_id=10023">Peter Hodge</a></i></td>
    <td class="rowodd" valign="top" width="2000">Alpha support for regular expressions inside preg_*() functions, plus some very minor additions.</td>
</tr>
<tr>
        <td class="roweven" valign="top" nowrap><a href="download_script.php?src_id=5816">php.vim</a></td>
    <td class="roweven" valign="top" nowrap><b>0.9</b></td>
    <td class="roweven" valign="top" nowrap><i>2006-06-08</i></td>
    <td class="roweven" valign="top" nowrap>6.0</td>
    <td class="roweven" valign="top"><i><a href="/account/profile.php?user_id=10023">Peter Hodge</a></i></td>
    <td class="roweven" valign="top" width="2000">Initial upload</td>
</tr>
</table>
<small>ip used for rating: 181.62.53.128</small>
<!-- finish off the framework -->
          </td>
        </tr>
      </table>
    </td>

  </tr>
</table>

<!-- END OF THE PAGE BODY: BETWEEN HEADER AND FOOTER -->

<table width="100%" cellpadding="0" cellspacing="0" border="0" bordercolor="red">
  <tr><td colspan="4"><img src="/images/spacer.gif" width="1" height="5" alt=""></td></tr>
  <tr><td colspan="4" bgcolor="#000000"><img src="/images/spacer.gif" height="2" width="1" alt=""></td></tr>
  <tr><td colspan="4"><img src="/images/spacer.gif" width="1" height="5" alt=""></td></tr>
  <tr>
    <td><img src="/images/spacer.gif" width="5" height="1" alt=""></td>

    <td align="left" valign="top"><small>
      If you have questions or remarks about this site, visit the
      <a href="http://vimonline.sf.net">vimonline development</a> pages.
      Please use this site responsibly.
      <br> 
      
      Questions about <a href="http://www.vim.org/about.php">Vim</a> should go
      to the <a href="http://www.vim.org/maillist.php">maillist</a>.
      Help Bram <a href="http://iccf-holland.org/">help Uganda</a>.
      </small>
	&nbsp;
	&nbsp;

    </td>

    <td align="right" valign="top">
      	<a href="https://osdn.net/projects/vim"><img src="https://osdn.net/sflogo.php?group_id=8&type=1" width="96" height="29" border="0" alt="OSDN.net Logo" /></a>
    </td>

    <td><img src="/images/spacer.gif" width="5" height="1" alt=""></td>
  </tr>

    
  <tr><td colspan="4"><img src="/images/spacer.gif" width="1" height="5" alt=""></td>
  
  </tr>
</table>

</body>
</html>

